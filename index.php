<!DOCTYPE html>
        <html>
<head>
    <title>Reaction tester Javascript game</title>

    <style>
        body {
            background-color: black;
            padding: 20px;
        }
        #container {
            margin: 0 auto;
            width: 600px;
            height: 600px;
            background-color: #c4c0b8;
            padding: 20px;
            box-shadow: 5px 5px 5px #606060;
            border-radius: 15px;
            border: 2px dashed #ff1a52;
        }
        #shape {
            width: 100px;
            height: 100px;
            background: red;
            display: none;
            border: 2px solid tomato;
            position: relative;
        }
        #area {
            background-color: #9d9991;
            box-shadow: inset 1px 1px 2px 3px rgba(0,0,0,.3);
            margin-top: 10px;
            width: 600px;
            height: 450px;
            border-radius: 10px;
        }
        #left, #right {
            float: left;
            width: 50%;
        }
        #clearfix {
            clear: both;
        }
        #reset {
            font-weight: normal;
            border-radius: 3px;
            border: 1px solid #0c0800;
            background-color: #919191;
            padding: 5px;
            margin-left: 150px;
        }
        a, a:link, a:visited, a:active, a:hover {
            text-decoration: none;
            color: #222;
        }
        #hidden {
            display: none;
        }
    </style>

</head>

<body>
    <div id="container">
        <h2>Reaction Tester <a href="index.html"><span id="reset">Reset</span></a></h2>

        <p>Click on the shapes below as quickly as you can!<span id="hidden">0</span></p>
        <div id="infopanel">
            <div id="left">
                <div style="font-weight:bold">Your Time: <span id="time">0</span>s</div>
                <div style="font-style:italic">Best Time: <span id="best">60</span>s</div>
            </div>
            <div id="right">
                <div>Clicks: <span id="clicks">0</span></div>
                <div style="font-style:oblique">Average: <span id="avg">60</span>s</div>
            </div>
        </div>
        <div id="clearfix"></div>
        <div id="area">

            <div id="shape"></div>

        </div>
    </div>
    <script type="text/javascript" language="JavaScript">
        var creationTime, clickedTime, reactionTime, bestTime;

        function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        var time = Math.random();
        time = time * 5000;

        function makeBox() {
            setTimeout(function(){
                if (Math.random()<0.5) {
                    document.getElementById('shape').style.borderRadius="50px";
                } else {
                    document.getElementById('shape').style.borderRadius="0";
                }
                var top=Math.random();
                top = top*360;
                var left=Math.random();
                left = left*500;
                document.getElementById('shape').style.top=top+"px";
                document.getElementById('shape').style.left=left+"px";
                document.getElementById('shape').style.backgroundColor=getRandomColor();
                document.getElementById('shape').style.display="block";
                creationTime=Date.now();
            }, time)
        }

        document.getElementById('shape').onclick=function(){
            clickedTime=Date.now();
            reactionTime=(clickedTime-creationTime)/1000;
            oldTime=document.getElementById('best').innerHTML;
            if (reactionTime<oldTime){
                bestTime=reactionTime;
                document.getElementById("best").innerHTML=bestTime;
            }
            document.getElementById("time").innerHTML=reactionTime;
            this.style.display="none";

            var clicks=document.getElementById('clicks').innerHTML;
            clicks = parseInt(clicks) + 1;
            document.getElementById('clicks').innerHTML=clicks;
            var hidden=document.getElementById('hidden').innerHTML;
            var hidden = parseFloat(hidden) + reactionTime;
            document.getElementById('hidden').innerHTML=hidden;
            var avg = hidden / clicks;
            avg = Math.round(avg * 100) / 100;
            document.getElementById('avg').innerHTML=avg;
            makeBox();
        }

        document.getElementById('area').ondblclick=function(){
                alert("Don't cheat by repeating clicks!");
        }

        makeBox();

    </script>
</body>
</html>